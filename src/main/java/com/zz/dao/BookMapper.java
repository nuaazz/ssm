package com.zz.dao;

import com.zz.pojo.Books;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookMapper {
    //增
    int addBook(Books book);
    //删
    int deleteBookById(@Param("bookID") int id);
    //改
    int updateBook(Books book);

    //查一本书
    Books queryBookById(@Param("bookID") int id);
    //查所有书
    List<Books> queryAllBook();

    Books queryBookByName(@Param("bookName") String bookName);

}
