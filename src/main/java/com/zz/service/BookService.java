package com.zz.service;

import com.zz.pojo.Books;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookService {
    //增
    int addBook(Books book);
    //删
    int deleteBookById( int id);
    //改
    int updateBook(Books book);

    //查一本书
    Books queryBookById( int id);
    //查所有书
    List<Books> queryAllBook();

    Books queryBookByName(String bookName);
}
