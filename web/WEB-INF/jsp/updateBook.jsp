<%--
  Created by IntelliJ IDEA.
  User: ZZ
  Date: 2020/8/24
  Time: 17:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改书籍</title>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>修改书籍</small>
                </h1>
            </div>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/book/updateBook" method="post">
<%--       前端传递隐藏域--%>
        <input type="hidden" name="bookID" value="${book.bookID}">
        书籍名称：<input type="text" name="bookName" value="${book.bookName}" required><br><br><br>
        书籍数量：<input type="text" name="bookCounts" value="${book.bookCounts}" required><br><br><br>
        书籍详情：<input type="text" name="detail" value="${book.detail}" required><br><br><br>
        <input type="submit" value="修改">
    </form>

</div>
</body>
</html>
